import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { Paper, Button, Grid, TextareaAutosize, Typography } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import racesImg from "./imgs/races.png";
import { profs, statsRace, skillsRace, skillsMap, abilitiesRace, professionPool, humanRandomSkills, halfingRandomSkills } from "./data";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import axios from "axios";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}

const photos = importAll(require.context("./imgs", false, /\.(png|jpe?g|svg)$/));

const races = ["Człowiek", "Elf", "Niziołek", "Krasnolud"];
const gender = ["Mężczyzna", "Kobieta"];

export default class Create extends Component {
  state = {
    experience: 100,
    stats: {
      WW: 0,
      US: 0,
      K: 0,
      Odp: 0,
      Zr: 0,
      Int: 0,
      SW: 0,
      Ogd: 0,
      A: 0,
      Żyw: 0,
      Sz: 0,
      Mag: 0,
      Po: 0,
      PP: 0,
    },
    upgrades: {
      WW: 0,
      US: 0,
      K: 0,
      Odp: 0,
      Zr: 0,
      Int: 0,
      SW: 0,
      Ogd: 0,
      A: 0,
      Żyw: 0,
      Sz: 0,
      Mag: 0,
      Po: 0,
      PP: 0,
      S: 0,
      Wt: 0,
    },
    goldKorons: 0,
    silverSzylings: 0,
    brownPennies: 0,
    specialSigns: "",
    zodiak: "Nie wie",
    index: 0,
    szalyja: false,
    skillChoice: [],
    abiChoice: [],
    eqChoice: [],
  };
  onChange = prop => event => {
    this.setState({
      [prop]: event.target.value,
    });
  };
  onChangeProf = (prop, value) => _ => {
    this.setState({
      [prop]: value,
    });
  };

  onPrev = () => {
    if (this.state.index === 0) {
      this.setState({
        index: profs.length - 1,
      });
    } else {
      this.setState({
        index: this.state.index - 1,
      });
    }
  };
  onNext = () => {
    if (this.state.index === profs.length - 1) {
      this.setState({
        index: 0,
      });
    } else {
      this.setState({
        index: this.state.index + 1,
      });
    }
  };

  makeStats = () => {
    function randomIntFromInterval(min, max) {
      // min and max included
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    var statsNew = {
      WW: 0,
      US: 0,
      K: 0,
      Odp: 0,
      Zr: 0,
      Int: 0,
      SW: 0,
      Ogd: 0,
      A: 0,
      Żyw: 0,
      Sz: 0,
      Mag: 0,
      Po: 0,
      PP: 0,
    };
    statsRace[this.state.race].forEach((el, i) => {
      var [key, _] = stats[i].split(" - ");
      if (typeof el === "object") {
        statsNew[key] = el[randomIntFromInterval(0, 9)];
      } else if (el === 0) {
        if (key === "S") {
          statsNew[key] = Math.floor(statsNew.K / 10);
        } else if (key === "Wt") {
          statsNew[key] = Math.floor(statsNew.Odp / 10);
        } else {
          statsNew[key] = 0;
        }
      } else if (i < 8) {
        statsNew[key] = el + randomIntFromInterval(1, 10) + randomIntFromInterval(1, 10);
      } else {
        statsNew[key] = el;
      }
    });
    var profData = professionPool[this.state.currentProfesion];
    var profSkills = profData.skills.split(", ");
    var profAbis = profData.abilities.split(", ");
    var profEq = profData.equipment.split(", ");
    let skillsNew = skillsRace[this.state.race];
    let abiNew = abilitiesRace[this.state.race];
    let eqNew = [];
    let skillChoice = [];
    let abiChoice = [];
    let eqChoice = [];

    profSkills.forEach(el => {
      if (el.indexOf(" albo ") === -1) {
        skillsNew.push(el);
      } else {
        skillChoice.push(el.split(" albo "));
      }
    });
    profAbis.forEach(el => {
      if (el.indexOf(" albo ") === -1) {
        abiNew.push(el);
      } else {
        abiChoice.push(el.split(" albo "));
      }
    });
    profEq.forEach(el => {
      if (el.indexOf(" albo ") === -1) {
        eqNew.push(el);
      } else {
        eqChoice.push(el.split(" albo "));
      }
    });

    this.setState({
      disabled: true,
      stats: statsNew,
      abilities: abiNew,
      skills: skillsNew,
      equipment: eqNew.join(", "),
      skillChoice: skillChoice,
      abiChoice: abiChoice,
      eqChoice: eqChoice,
    });
  };

  onAddArr = event => {
    var data = event.target.dataset;
    if (event.target.nodeName === "SPAN") {
      data = event.target.parentElement.dataset;
    }
    var newArr = [...this.state[data.d]];
    newArr.splice(data.c, 1);
    this.setState({
      [data.a]: [...this.state[data.a], data.b],
      [data.d]: newArr,
    });
  };

  onJoin = event => {
    var data = event.target.dataset;
    if (event.target.nodeName === "SPAN") {
      data = event.target.parentElement.dataset;
    }
    var newArr = [...this.state.eqChoice];
    newArr.splice(data.c, 1);
    this.setState({
      [data.a]: this.state[data.a] + ", " + data.b,
      eqChoice: newArr,
    });
  };

  addValue = event => {
    var data = event.target.dataset;
    if (event.target.nodeName === "SPAN") {
      data = event.target.parentElement.dataset;
    }
    var newStats = Object.assign(this.state.stats);
    newStats[data.a] = Number.parseInt(data.b);

    if (data.a === "K") {
      newStats.S = Math.floor(newStats[data.a] / 10);
    } else if (data.a === "Odp") {
      newStats.Wt = Math.floor(newStats[data.a] / 10);
    }
    this.setState({
      stats: newStats,
      szalyja: true,
    });
  };

  save = () => {
    var newChar = Object.assign(this.state);
    delete newChar.index;
    delete newChar.szalyja;
    delete newChar.skillChoice;
    delete newChar.abiChoice;
    delete newChar.eqChoice;

    axios
      .post("http://195.181.210.249:3000/wfrp/", {
        ...{
          author: localStorage.username,
          password: localStorage.password,
          session: "",
        },
        json: JSON.stringify(newChar),
      })
      .then(() => {
        window.location.reload();
      });
  };

  onChangeAuto = prop => event => {
    var arr = this.state[prop].trim().split(", ");
    console.log(event.target.nodeName);
    if (event.target.textContent.trim()) {
      var index = arr.indexOf(event.target.textContent.trim());
      if (index !== -1) {
        arr.splice(index, 1);
      } else {
        arr.push(event.target.textContent.trim());
      }
    } else if (event.target.nodeName === "path" || event.target.nodeName === "svg") {
      var parent = event.target.parentElement;
      if (event.target.nodeName === "path") {
        parent = parent.parentElement;
      }
      arr.splice(arr.indexOf(parent.textContent.trim()), 1);
    }
    arr = arr.filter(el => el);
    this.setState({
      [prop]: arr.join(", "),
    });
  };
  render() {
    return (
      <Grid direction="column" container>
        {!this.state.race && <img src={racesImg} alt="tabelka" />}
        <TextField id="race" select label="Rasa" value={this.state.race} onChange={this.onChange("race")} disabled={this.state.disabled}>
          {races.map(option => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        <TextField id="gender" select label="Płeć" value={this.state.gender} onChange={this.onChange("gender")} disabled={this.state.disabled}>
          {gender.map(option => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        {this.state.race && this.state.gender && (
          <>
            <TextField id="name" select label="Imię" value={this.state.name} onChange={this.onChange("name")}>
              {names[this.state.race + this.state.gender].filter(onlyUnique).map(option => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>

            <TextField id="age" input label={`Wiek (${ages[this.state.race + this.state.gender]})`} value={this.state.age} onChange={this.onChange("age")} />
            <TextField input label={`Waga (${weights[this.state.race + this.state.gender]}kg)`} value={this.state.weight} onChange={this.onChange("weight")} />
            <TextField
              input
              label={`Wzrost (${growths[this.state.race + this.state.gender]}cm)`}
              value={this.state.growth}
              onChange={this.onChange("growth")}
            />
            <TextField input label={`Kolor oczu`} value={this.state.eyeColor} onChange={this.onChange("eyeColor")} />
            <TextField input label={`Kolor włosów`} value={this.state.hairColor} onChange={this.onChange("hairColor")} />
            <TextField input label={`Styl uczesania`} value={this.state.hairStyle} onChange={this.onChange("hairStyle")} />
            <div className="auto-root">
              <Autocomplete
                disableClearable
                multiple
                id="tags-standard"
                options={signs}
                getOptionLabel={sign => sign}
                defaultValue={[]}
                onChange={this.onChangeAuto("specialSigns")}
                renderInput={params => <TextField {...params} variant="standard" label={`Znaki szczególne- co najmniej 2`} />}
              />
            </div>
            <TextField select label="Pochodzenie" value={this.state.city} onChange={this.onChange("city")}>
              {cities.filter(onlyUnique).map(option => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
            <TextField select label="Wielkoś miejscowości" value={this.state.cityAdd} onChange={this.onChange("cityAdd")}>
              {cityLarge.filter(onlyUnique).map(option => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
            {!this.state.city && <img src={"https://i.pinimg.com/originals/e4/a6/a1/e4a6a1c50107569a05a4b53a6245e63d.jpg"} />}

            {!this.state.currentProfesion && (
              <>
                <Typography>
                  <b>Wybor profesji</b>
                </Typography>
                <Grid container direction="row" justify="center" alignItems="center">
                  <Button variant="contained" color="primary" onClick={this.onPrev}>
                    Poprzednia
                  </Button>
                  <Button variant="contained" color="secondary" onClick={this.onNext}>
                    Następna
                  </Button>
                </Grid>
                <Grid container direction="column" justify="center" alignItems="center">
                  <Button variant="contained" color="primary" onClick={this.onChangeProf("currentProfesion", profs[this.state.index])}>
                    WYBIERZ: {profs[this.state.index]}
                  </Button>
                  <div>
                    <img src={photos[`profession${profs.indexOf(profs[this.state.index]) + 1}.png`]} />
                    <p className="legend">{profs[this.state.index]}</p>
                  </div>
                </Grid>
              </>
            )}
            {this.state.currentProfesion && (
              <>
                <Grid container direction="column" justify="center" alignItems="center">
                  <Typography>
                    <b>{this.state.currentProfesion}</b>
                  </Typography>
                  <Button disabled={this.state.disabled} variant="contained" color="primary" onClick={this.makeStats}>
                    {" "}
                    Losuj moje SIŁY!
                  </Button>
                </Grid>
                {this.state.abiChoice.map((el, i) => (
                  <Grid container direction="column" justify="center" alignItems="center">
                    <Typography>
                      <b>Wybierz zdolnosc</b>
                    </Typography>
                    <Grid container direction="row" justify="center" alignItems="center">
                      <Button variant="contained" color="primary" data-a="abilities" data-b={el[0]} data-c={i} data-d="abiChoice" onClick={this.onAddArr}>
                        {el[0]}
                      </Button>
                      <Typography>
                        <b> Albo </b>
                      </Typography>
                      <Button variant="contained" color="secondary" data-a="abilities" data-b={el[1]} data-c={i} data-d="abiChoice" onClick={this.onAddArr}>
                        {el[1]}
                      </Button>
                    </Grid>
                  </Grid>
                ))}

                {this.state.skillChoice.map((el, i) => (
                  <Grid container direction="column" justify="center" alignItems="center">
                    <Typography>
                      <b>Wybierz umiejetnosc</b>
                    </Typography>
                    <Grid container direction="row" justify="center" alignItems="center">
                      <Button variant="contained" color="primary" data-a="skills" data-b={el[0]} data-c={i} data-d="skillChoice" onClick={this.onAddArr}>
                        {el[0]}
                      </Button>
                      <Typography>
                        <b> Albo </b>
                      </Typography>
                      <Button variant="contained" color="secondary" data-a="skills" data-b={el[1]} data-c={i} data-d="skillChoice" onClick={this.onAddArr}>
                        {el[1]}
                      </Button>
                    </Grid>
                  </Grid>
                ))}

                {this.state.eqChoice.map((el, i) => (
                  <Grid container direction="column" justify="center" alignItems="center">
                    <Typography>
                      <b>Wybierz ekwipunek</b>
                    </Typography>
                    <Grid container direction="row" justify="center" alignItems="center">
                      <Button variant="contained" color="primary" data-a="equipment" data-b={el[0]} data-c={i} onClick={this.onJoin}>
                        {el[0]}
                      </Button>
                      <Typography>
                        <b> Albo </b>
                      </Typography>
                      <Button variant="contained" color="secondary" data-a="equipment" data-b={el[0]} data-c={i} onClick={this.onJoin}>
                        {el[1]}
                      </Button>
                    </Grid>
                  </Grid>
                ))}
                {this.state.disabled && (
                  <TableContainer component={Paper}>
                    <Table className="table" aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Statystyka</TableCell>
                          <TableCell align="right">Początkowa</TableCell>
                          <TableCell align="right">Rozwinięcie</TableCell>
                          <TableCell align="right">Obecna</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {stats.map((row, i) => {
                          const [key, full] = row.split(" - ");
                          var value = this.state.stats[key];
                          var upgrade = this.state.upgrades ? this.state.upgrades[key] : 0;
                          return (
                            <TableRow key={key}>
                              <TableCell component="th" scope="row">
                                {full}
                              </TableCell>
                              <TableCell align="right">
                                {value}{" "}
                                {!this.state.szalyja && i < 8 && value < statsRace[this.state.race][i] + 11 && (
                                  <Button
                                    variant="contained"
                                    color="secondary"
                                    data-a={key}
                                    data-b={statsRace[this.state.race][i] + 11}
                                    onClick={this.addValue}>
                                    Łaska Shalyi
                                  </Button>
                                )}
                              </TableCell>
                              <TableCell align="right">+{upgrade}</TableCell>
                              <TableCell align="right">{value + upgrade}</TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    </Table>
                  </TableContainer>
                )}
              </>
            )}

            <TextareaAutosize
              rowsMin={3}
              className="area"
              aria-label="empty textarea"
              placeholder="Relacje rodzinne "
              onChange={this.onChange("familyRelations")}
            />
            <TextareaAutosize rowsMin={3} className="area" aria-label="empty textarea" placeholder="Pozycja społeczna " onChange={this.onChange("position")} />
            <TextareaAutosize rowsMin={3} className="area" aria-label="empty textarea" placeholder="Historia " onChange={this.onChange("history")} />
            <TextareaAutosize
              rowsMin={3}
              className="area"
              aria-label="empty textarea"
              placeholder="Dlaczego wyruszył na szlak "
              onChange={this.onChange("motivation")}
            />
            <TextareaAutosize
              rowsMin={3}
              className="area"
              aria-label="empty textarea"
              placeholder="Jak sam siebie opisuje "
              onChange={this.onChange("aboutHimself")}
            />
            <TextareaAutosize rowsMin={3} className="area" aria-label="empty textarea" placeholder="Czego nienawidzi" onChange={this.onChange("mostHate")} />
            <TextareaAutosize rowsMin={3} className="area" aria-label="empty textarea" placeholder="Do czego zmierza? " onChange={this.onChange("goal")} />

            <Grid container direction="column" justify="center" alignItems="center">
              <Button variant="contained" color="secondary" onClick={this.save}>
                Zapisz
              </Button>
            </Grid>
          </>
        )}
      </Grid>
    );
  }
}

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

var stats = [
  "WW - Walka wręcz",
  "US - Umiejętności strzeleckie",
  "K - Krzepa",
  "Odp - Odporność",
  "Zr - Zręczność",
  "Int - Inteligencja",
  "SW - Siła woli",
  "Ogd - Ogłada",
  "A - Akcje",
  "Żyw - Życie",
  "S - Siła",
  "Wt - Wytrzymałość",
  "Sz - Szybkość",
  "Mag - Magia",
  "Po - Obłęd",
  "PP - Punkty przeznaczenia",
];

var NamesDwarfMale = [
  "Adrik",
  "Alberich",
  "Baern",
  "Barendd",
  "Brouor",
  "Bruenor",
  "Dain",
  "Darrak",
  "Delg",
  "Eberk",
  "Einkil",
  "Fargrim",
  "Flint",
  "Gardain",
  "Harbek",
  "Kildrak",
  "Morgran",
  "Orsik",
  "Oskar",
  "Rangrim",
  "Rurik",
  "Taklinn",
  "Thoradin",
  "Thorin",
  "Tordek",
  "Traubon",
  "Travok",
  "Ulfgar",
  "Veit",
  "Vondal",
  "Alrik",
  "Bronda",
  "Dimzad",
  "Fenna",
  "Gottri",
  "Gudrun",
  "Snorri",
];
var NamesHalflingMale = [
  "Alton",
  "Ander",
  "Cade",
  "Corrin",
  "Eldon",
  "Errich,",
  "Finnan",
  "Garret",
  "Lindal",
  "Lyle",
  "Merrie",
  "Milo",
  "Osborn",
  "Perrin",
  "Reed",
  "Roscoe",
  "Wellby",
  "Ferdinand",
  "Heironymus",
  "Maximillian",
  "Theodosius",
];
var NamesElfMale = [
  "Adran",
  "Aelar",
  "Aramil",
  "Arannis",
  "Aust",
  "Beiro",
  "Berrian",
  "Carrie",
  "Enialis",
  "Erdan",
  "Erevan",
  "Galinndan",
  "Hadarai",
  "Heian",
  "Himo",
  "Immeral",
  "Ivellios",
  "Laucian",
  "Mindartis",
  "Paelias",
  "Peren",
  "Quarion",
  "Riardol",
  "Rolen",
  "Soveliss",
  "Thamior",
  "Tharivol",
  "Theren",
  "Varis",
];
var NamesGnomeMale = ["Breward", "Daveth", "Gwinear", "Mawnan", "Meriasek", "Nivet", "Talan", "Ytel"];
var NamesHumanFemale = [
  "Andrea",
  "Angelika",
  "Anita",
  "Anna-Lise",
  "Astrid",
  "Brigid",
  "Brigitte",
  "Camilla",
  "Celeste",
  "Charlotte",
  "Christa",
  "Christiane",
  "Clementine",
  "Cordula",
  "Daniele",
  "Doris",
  "Eva",
  "Edda",
  "Edith",
  "Elga",
  "Elisabeth",
  "Elixabet",
  "Ellen",
  "Etti",
  "Frances",
  "Frieda",
  "Gabriel e",
  "Gaby",
  "Gerd",
  "Gertrud",
  "Gisela",
  "Gloria",
  "Hanna",
  "Hanni",
  "Hannelore",
  "Heidi",
  "Helena",
  "Helene",
  "Helga",
  "Helke",
  "Helma",
  "Henriette",
  "Ingrid",
  "Karin",
  "Karina",
  "Katharina",
  "Kathe",
  "Katrin",
  "Julianne",
  "Lena",
  "Lile",
  "Lilian",
  "Lisa",
  "Liv",
  "Magdalene",
  "Margret",
  "Margarethe",
  "Magrit",
  "Maria",
  "Mariann",
  "Marianne",
  "Marlene",
  "Marthe",
  "Mel ory",
  "Monika",
  "Nadja",
  "Nastassja",
  "Natja",
  "Oliva",
  "Rena",
  "Rosa",
  "Ruth",
  "Sybil e",
  "Tatjana",
  "Uli",
  "Ursula",
  "Vera",
  "Veronika",
  "Winifred",
  "Yella",
  "Yvette",
  "Dagmar",
  "Adelheid",
  "Heide",
  "Agathe",
  "Agatha",
  "Agnes",
  "Alexandra",
  "Alexa",
  "Alix",
  "Andrea",
  "Angelika",
  "Anna",
  "Anne",
  "Anneliese",
  "Annette",
  "Antonia",
  "Augusta",
  "Beata",
  "Beate",
  "Bertha",
  "Berthe",
  "Brigitte",
  "Gitte",
  "Britta",
  "Cacilie",
  "Camila",
  "Carola",
  "Karola",
  "Charlotte",
  "Lotte",
  "Claudia",
  "Clementine",
  "Cordula",
  "Cornelia",
  "Kornelia",
  "Daniela",
  "Dora",
  "Doris",
  "Dorothea",
  "Edda",
  "Edith",
  "Eleonore",
  "Elisabeth",
  "Elsbeth",
  "Elsa",
  "Lisbeth",
  "Elke",
  "Elen",
  "Else",
  "Elvira",
  "Emilie",
  "Emilia",
  "Erika",
  "Esther",
  "Eugenie",
  "Eva",
  "Felicitas",
  "Franka",
  "Franza",
  "Franziska",
  "Franzi",
  "Frieda",
  "Friederike",
  "Gabriele",
  "Gerda",
  "Gertraud",
  "Gertraut",
  "Gertrude",
  "Gisela",
  "Gloria",
  "Hanna",
  "Hannelore",
  "Hedwig",
  "Helene",
  "Helena",
  "Helga",
  "Helma",
  "Henriette",
  "Hildegard",
  "Hilde",
  "Ida",
  "Ilse",
  "Ina",
  "Ingrid",
  "Isabela",
  "Isadora",
  "Isolde",
  "Johanna",
  "Josephine",
  "Judith",
  "Jutta",
  "Juliane",
  "Julia",
  "Karin",
  "Karla",
  "Karoline",
  "Katharina",
  "Kathe",
  "Kathrin",
  "Leonore",
  "Liliane",
  "Lore",
  "Luise",
  "Luzia",
  "Magda",
  "Magdalene",
  "Lene",
  "Lena",
  "Margarete",
  "Margareta",
  "Gretel",
  "Margit",
  "Margot",
  "Marie",
  "Marianne",
  "Marion",
  "Marlene",
  "Martha",
  "Marthe",
  "Mathilde",
  "Michaela",
  "Monika",
  "Nadja",
  "Ottilie",
  "Paula",
  "Petra",
  "Rebekka",
  "Regina",
  "Regine",
  "Renate",
  "Renata",
  "Rikarda",
  "Rosa",
  "Ruth",
  "Sabine",
  "Sibyle",
  "Silvia",
  "Sophie",
  "Stefanie",
  "Stephanie",
  "Susanne",
  "Susanna",
  "Therese",
  "Ulrike",
  "Ursula",
  "Ursel",
  "Valentina",
  "Verena",
  "Vera",
  "Veronika",
  "Viktoria",
  "Wilhelmine",
  "Winifred",
];
var NamesDwarfFemale = [
  "Amber",
  "Artin",
  "Audhild",
  "Bardryn",
  "Dagnal",
  "Diesa",
  "Eldeth",
  "Falkrunn",
  "Finellen",
  "Gunnloda",
  "Gurdis",
  "Helja",
  "Hlin",
  "Kathra",
  "Kristryd",
  "Hilde",
  "Liftrasa",
  "Mardred",
  "Riswynn",
  "Sannl",
  "Torbera",
  "Torgga",
  "Vistra",
];
var NamesHalflingFemale = [
  "Andry",
  "Bree",
  "Callie",
  "Cora",
  "Euphemia",
  "Jillian",
  "Kithri",
  "Lavinia",
  "Lidda",
  "Meda",
  "Nedda",
  "Paela",
  "Portia",
  "Seraphina",
  "Shaena",
  "Trym",
  "Vani",
  "Verna",
  "Antoniella",
  "Esmerelda",
  "Thomasina",
];
var NamesElfFemale = [
  "Adrie",
  "Althaea",
  "Anastrianna,",
  "Andraste",
  "Antinua",
  "Bethrynna",
  "Birel",
  "Caelynn,",
  "Drusilia",
  "Enna",
  "Felosial",
  "Ielenia",
  "Jelenneth",
  "Keyleth",
  "Leshanna",
  "Lia",
  "Meriele",
  "Mialee",
  "Naivara",
  "Quelenna",
  "Quillathe",
  "Sariel",
  "Shanairra",
  "Shava",
  "Silaqui",
  "Theirastra",
  "Thia",
  "Vadania",
  "Valanthe",
  "Xanaphia",
];
var NamesHumanMale = [
  "Armin",
  "Adolf",
  "Adolphus",
  "Albrecht",
  "Alexander",
  "Aldred",
  "Alfred",
  "Anders",
  "Anton",
  "Axel",
  "Bernard",
  "Bernd",
  "Bert",
  "Boris",
  "Bruno",
  "Chedwic",
  "Christian",
  "Christoph",
  "Claus",
  "Clemons",
  "Conrad",
  "Konrad",
  "Dahlbert",
  "Detlef",
  "Dieter",
  "Dirk",
  "Erberhardt",
  "Erik",
  "Erich",
  "Ernst",
  "Erwin",
  "Erzbet",
  "Felix",
  "Franz",
  "Friedrich",
  "Fredrick",
  "Fritz",
  "Georg",
  "Gerd",
  "Gotz",
  "Gregor",
  "Gunter",
  "Gunther",
  "Gustav",
  "Hannes",
  "Hans",
  "Hanzi",
  "Heiner",
  "Heinrich",
  "Heinz",
  "Hieronymus",
  "Helmut",
  "Henri",
  "Holger",
  "Ingo",
  "Jens",
  "Joachim",
  "Johann",
  "Johannes",
  "Jonas",
  "Jorg",
  "Josef",
  "Jurgen",
  "Kaster",
  "Karl",
  "Klaus",
  "Knud",
  "Konrad",
  "Kurt",
  "Lou",
  "Luitpold",
  "Manfred",
  "Marius",
  "Max (Maximilian)",
  "Mathieu",
  "Matthias",
  "Mikhail",
  "Mortmore",
  "Nathaniel",
  "Nicholas",
  "Nicholaus",
  "Nikolaus",
  "Norbert",
  "Oswald",
  "Otto",
  "Paulus",
  "Peter",
  "Rainer",
  "Reiner",
  "Rolf",
  "Rudger",
  "Rudolf",
  "Rudolph",
  "Ruy",
  "Sepp",
  "Siegfried",
  "Stefan",
  "Sven",
  "Thadius",
  "Theodor",
  "Udo",
  "Ulrich",
  "Ulrike",
  "Volker",
  "Werner",
  "Wiesel",
  "Wilhelm",
  "Wim",
  "Wolf",
  "Wolfgang",
  "Adalbert",
  "Adam",
  "Adolf",
  "Adolphus",
  "Albert",
  "Albrecht",
  "Aldred",
  "Alexander",
  "Axel",
  "Alfons",
  "Alfred",
  "Alois",
  "Aloisius",
  "Andreas",
  "Anders",
  "Anton",
  "Antonius",
  "Armin",
  "Arnold",
  "Arthur",
  "August",
  "Augustus",
  "Balthasar",
  "Benedikt",
  "Bernhard",
  "Benno",
  "Bernd",
  "Berthold",
  "Bartolomeus",
  "Bertram",
  "Bonifatius",
  "Clemens",
  "Cyrilus",
  "Daniel",
  "David",
  "Detlef",
  "Dettel",
  "Dieter",
  "Dieterich",
  "Dirk",
  "Eberhard",
  "Edgar",
  "Edler",
  "Edmund",
  "Eduard",
  "Ede",
  "Egolf",
  "Egon",
  "Eitel",
  "Elmar",
  "Elmer",
  "Emil",
  "Emilius",
  "Erhard",
  "Erich",
  "Erik",
  "Ernst",
  "Erwin",
  "Eugen",
  "Fabian",
  "Felix",
  "Ferdinand",
  "Ferdl",
  "Florian",
  "Frank",
  "Franz",
  "Francius",
  "Friedrich",
  "Frieder",
  "Fritz",
  "Gabriel",
  "Georg",
  "Gerhard",
  "Gerd",
  "Götz",
  "Gregor",
  "Gunter",
  "Gustav",
  "Gustavus",
  "Harald",
  "Heinrich",
  "Heiner",
  "Heinz",
  "Herbert",
  "Heribert",
  "Hermann",
  "Arminius",
  "Hieronymus",
  "Holger",
  "Hubert",
  "Hugo",
  "Ignaz",
  "Ignatius",
  "Isidor",
  "Jakob",
  "Jacobus",
  "Joachim",
  "Achim",
  "Jochen",
  "Johannes",
  "Hans",
  "Hannes",
  "Jonas",
  "Jonathan",
  "Jorg",
  "Josef",
  "Josephus",
  "Sepp",
  "Julius",
  "Jurgen",
  "Karl",
  "Carolus",
  "Kasimir",
  "Kaster",
  "Castor",
  "Kaspar",
  "Kasper",
  "Caspar",
  "Knut",
  "Konrad",
  "Conrad",
  "Konz",
  "Konstantin",
  "Kurt",
  "Laurenz",
  "Lorenz",
  "Laurentius",
  "Leonhard",
  "Leo",
  "Ludwig",
  "Lodeweik",
  "Luitpold",
  "Lutz",
  "Manfred",
  "Marius",
  "Markus",
  "Martin",
  "Mathias",
  "Mattheus",
  "Maximilian",
  "Max",
  "Melchior",
  "Michael",
  "Michel",
  "Moritz",
  "Nathan",
  "Nikolaus",
  "Niklas",
  "Klaus",
  "Norbert",
  "Olaf",
  "Oskar",
  "Oswald",
  "Otto",
  "Paul",
  "Paulus",
  "Peter",
  "Petrus",
  "Philipp",
  "Philippus",
  "Pius",
  "Raimund",
  "Reimund",
  "Rainer",
  "Reiner",
  "Richard",
  "Robert",
  "Bert",
  "Roland",
  "Rudiger",
  "Rudger",
  "Roger",
  "Rudolf",
  "Rolf",
  "Rudi",
  "Ruprecht",
  "Rupert",
  "Samuel",
  "Siegfried",
  "Silvester",
  "Simon",
  "Stefan",
  "Stephan",
  "Steffen",
  "Sven",
  "Thadeus",
  "Theodor",
  "Theo",
  "Thomas",
  "Timotheus",
  "Titus",
  "Tobias",
  "Udo",
  "Ulrich",
  "Valentin",
  "Veit",
  "Viktor",
  "Volkmar",
  "Volker",
  "Walter",
  "Wendel",
  "Werner",
  "Wolfgang",
  "Wolf",
  "Wilhelm",
  "Wili",
  "Zacharias",
];

var names = {
  CzłowiekMężczyzna: NamesHumanMale,
  CzłowiekKobieta: NamesHumanFemale,
  KrasnoludMężczyzna: NamesDwarfMale,
  KrasnoludKobieta: NamesDwarfFemale,
  ElfMężczyzna: NamesElfMale,
  ElfKobieta: NamesElfFemale,
  NiziołekMężczyzna: NamesHalflingMale,
  NiziołekKobieta: NamesHalflingFemale,
};

var ages = {
  CzłowiekMężczyzna: "16- 35",
  CzłowiekKobieta: "16- 35",
  KrasnoludMężczyzna: "20- 115",
  KrasnoludKobieta: "20- 115",
  ElfMężczyzna: "20- 125",
  ElfKobieta: "20- 125",
  NiziołekMężczyzna: "20- 60",
  NiziołekKobieta: "20- 60",
};
var growths = {
  CzłowiekMężczyzna: "160- 190",
  CzłowiekKobieta: "150- 180",
  KrasnoludMężczyzna: "140- 160",
  KrasnoludKobieta: "130- 155",
  ElfMężczyzna: "170- 200",
  ElfKobieta: "165- 195",
  NiziołekMężczyzna: "100- 125",
  NiziołekKobieta: "95- 120",
};
var weights = {
  CzłowiekMężczyzna: "50- 110",
  CzłowiekKobieta: "50- 110",
  KrasnoludMężczyzna: "45- 100",
  KrasnoludKobieta: "45- 100",
  ElfMężczyzna: "40- 95",
  ElfKobieta: "40- 95",
  NiziołekMężczyzna: "35- 70",
  NiziołekKobieta: "35- 70",
};

var signs = [
  "Bielmo na oku",
  "Blizna",
  "Brak brwi",
  "Brak palca",
  "Brak zęba",
  "Brodawki",
  "Blada cera",
  "Duży nos",
  "Duży pieprzyk",
  "Dziwny zapach ciała",
  "Kolczyk w nosie",
  "Kolczyk w uchu",
  "Niewielka łysina",
  "Oczy różnego koloru",
  "Piegi",
  "Poszarpane ucho",
  "Ślady po ospie",
  "Tatuaż",
  "Wystające zęby",
  "Wytrzeszczone oczy",
  "Złamany nos",
];

var cityLarge = [
  "Stolica prowincji",
  "Bogate miasto",
  "Miasto targowe",
  "Fort wojskowy",
  "Miasteczko",
  "Bogata wieś",
  "WIoska rolnicza",
  "Wioska rybacka",
  "Biedna wioska",
  "Samotna chata",
];
var cities = [
  "Averland",
  "Hochland",
  "Middenland",
  "Nordland",
  "Ostermark",
  "Ostland",
  "Reikland",
  "Stirland",
  "Talabecland",
  "Wissenland",
  "Bretonia",
  "Tilea",
  "Norska",
  "Kislev",
  "Las Laurelorn",
  "Las Reikwald",
  "WIelki Las",
  "Kraina zgromadzenia",
  "Karak Norn",
  "Karak Izor",
  "Karak Hirn",
  "Karak Kadrin",
  "Karaz-a-Karak",
  "Zhufbar",
  "Bark Varr",
];
