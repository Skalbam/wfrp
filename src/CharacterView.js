import React, { useState } from "react";
import { Button, Grid, TextareaAutosize, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import { profs, statsRace, professionPool } from "./data";

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}

const photos = importAll(require.context("./imgs", false, /\.(png|jpe?g|svg)$/));

const useStyles = makeStyles({
  table: {
    minWidth: 400,
  },
  area: {
    fontSize: "21px",
    minWidth: 320,
  },
  input: {
    textAlign: "center !important",
  },
});

const stats = [
  "WW - Walka wręcz - 5",
  "US - Umiejętności strzeleckie - 5",
  "K - Krzepa - 5",
  "Odp - Odporność - 5",
  "Zr - Zręczność - 5",
  "Int - Inteligencja - 5",
  "SW - Siła woli - 5",
  "Ogd - Ogłada - 5",
  "A - Akcje - 1",
  "Żyw - Życie - 1",
  "S - Siła",
  "Wt - Wytrzymałość",
  "Sz - Szybkość",
  "Mag - Magia",
  "Po - Obłęd",
  "PP - Punkty przeznaczenia",
];

export default function CharacterView({ character, setToUpdate, full, isEdit }) {
  const classes = useStyles();
  const [char, setChar] = useState(character);
  var newChar = Object.assign(character);
  var i = 0;

  const onChange = event => {
    newChar[event.target.id] = event.target.value;
    setChar(newChar);
  };

  const addValue = event => {
    var data = event.target.dataset;
    if (event.target.nodeName === "SPAN") {
      data = event.target.parentElement.dataset;
    }

    var newStats = Object.assign(newChar.upgrades);
    newStats[data.a] = Number.parseInt(data.b);

    if (data.a === "K") {
      newStats.S = Math.floor(newStats[data.a] / 10) - newStats.S;
    } else if (data.a === "Odp") {
      newStats.Wt = Math.floor(newStats[data.a] / 10) - newStats.Wt;
    }
    newChar.upgrades = newStats;
    newChar.experience = Number.parseInt(newChar.experience) - 100;
      onSave();
      setToUpdate(newChar)
  };

  const onSave = () => {
    axios
      .put("http://195.181.210.249:3000/wfrp/" + full.id, {
        ...full,
        json: JSON.stringify(newChar),
      })
      .then(() => {
        setToUpdate(++i);
      });
  };
  return (
    <>
      <Grid container direction="column">
        <Button variant="contained" color="primary" onClick={onSave}>
          Zapisz
        </Button>
        <Grid direction="row" container spacing={3}>
          <Grid item direction="column" xs={6} spacing={3}>
            <Typography>
              imię: <b>{char.name}</b>
            </Typography>
            <Typography>
              rasa: <b>{char.race}</b>
            </Typography>
            <Typography>
              Profesja: <b>{char.currentProfesion}</b>
            </Typography>
            <Typography>
              Poprzednia profesja: <b>{char.previousProfesion ? char.previousProfesion : "---"}</b>
            </Typography>
            <Typography>
              Płeć: <b>{char.gender}</b>
            </Typography>
            <Typography>
              Wiek: <b>{char.age}</b>
            </Typography>
            <Typography>
              Wzrost: <b>{char.growth}cm</b>
            </Typography>
            <Typography>
              Waga: <b>{char.weight}kg</b>
            </Typography>
            <Typography>
              Kolor oczu: <b>{char.eyeColor}</b>
            </Typography>
            <Typography>
              Kolor włosów i uczesanie:{" "}
              <b>
                {char.hairColor} {char.hairStyle}
              </b>
            </Typography>
            <Typography>
              Znak gwiezdny: <b>{char.zodiak}</b>
            </Typography>
            <Typography>
              Znaki szczególne: <b>{char.specialSigns}</b>
            </Typography>

            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Statystyka</TableCell>
                    <TableCell align="right">Początkowa</TableCell>
                    <TableCell align="right">Rozwinięcie</TableCell>
                    <TableCell align="right">Obecna</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {stats.map(row => {
                    const [key, full, upgradeAbility] = row.split(" - ");
                    var value = Number.parseInt(char.stats[key]);
                    var upgrade = char.upgrades ? Number.parseInt(char.upgrades[key]) : 0;
                    if (key === "S") {
                      value = Math.floor(char.stats.K / 10);
                      upgrade = Math.floor((char.stats.K + (char.upgrades ? char.upgrades.K : 0)) / 10) - value;
                    } else if (key === "Wt") {
                      value = Math.floor(char.stats.Odp / 10);
                      upgrade = Math.floor((char.stats.Odp + (char.upgrades ? char.upgrades.Odp : 0)) / 10) - value;
                    }

                    return (
                      <TableRow key={key}>
                        <TableCell component="th" scope="row">
                          {full}
                        </TableCell>
                        <TableCell align="right">{value}</TableCell>
                        <TableCell align="right">
                          +{upgrade}
                          {isEdit && char.experience >= 100 &&
                            upgradeAbility &&
                            Number.parseInt(upgradeAbility) > 0 &&
                            professionPool[char.currentProfesion].stats[key] &&
                            professionPool[char.currentProfesion].stats[key] > char.upgrades[key] && (
                              <Button variant="contained" color="secondary" data-a={key} data-b={Number.parseInt(upgradeAbility)} onClick={addValue}>
                                +{Number.parseInt(upgradeAbility)}
                              </Button>
                            )}
                        </TableCell>
                        <TableCell align="right">{value + upgrade}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>

            <Typography>
              Umiejętności: <b>{char.abilities.join(", ")}</b>
            </Typography>
            <Typography>
              Zdolności: <b>{char.skills.join(", ")}</b>
            </Typography>

            <Typography>
              Miasto pochodzenia:{" "}
              <b>
                {char.city} - {char.cityAdd}
              </b>
            </Typography>
            <Typography>
              Sam o Sobie: <b>{char.aboutHimself}</b>
            </Typography>
            <Typography>
              Cel: <b>{char.goal}</b>
            </Typography>
            <Typography>
              Historia: <b>{char.history}</b>
            </Typography>
            <Typography>
              Nienawidzi: <b>{char.mostHate}</b>
            </Typography>
            <Typography>
              Pozycja społeczna: <b>{char.position}</b>
            </Typography>
            <Typography>
              Czemu wyruszył na szlak: <b>{char.motivation}</b>
            </Typography>
            <Typography>
              Relacje rodzinne: <b>{char.familyRelations}</b>
            </Typography>
          </Grid>
          <Grid item xs={6}>
              {localStorage.isMg && (
                  <Typography>
                      EXP: <TextField onChange={onChange} id="experience" defaultValue={char.experience} />
                  </Typography>
              )}
            <Typography>
              Złote korony: <TextField onChange={onChange} id="goldKorons" defaultValue={char.goldKorons} />
            </Typography>
            <Typography>
              Srebrne szyligni (20 = 1 zk): <TextField onChange={onChange} id="silverSzylings" defaultValue={char.silverSzylings} />
            </Typography>
            <Typography>
              Mosiężne pensy (240 = 1ss): <TextField onChange={onChange} id="brownPennies" defaultValue={char.brownPennies} />
            </Typography>
            <Typography>
              Wyposażenie: <TextareaAutosize onChange={onChange} className={classes.area} id="equipment" defaultValue={char.equipment} />
            </Typography>
            <Typography>
              Zbroja: <TextareaAutosize onChange={onChange} className={classes.area} id="armour" defaultValue={char.armour} />
            </Typography>
            <img src={photos[`profile${profs.indexOf(char.currentProfesion) + 1}.png`]} />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
