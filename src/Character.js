import React, { useState, useEffect } from "react";
import axios from "axios";
import Create from "./Create";
import CharacterView from "./CharacterView";

export default function Character(props) {
  const [character, setCharacter] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [isDone, setChange] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios.get("http://195.181.210.249:3000/wfrp/author/" + localStorage.username);
      if (res.data[0]) {
        setCharacter(res.data[0]);
      }
      setLoading(false);
    };
    fetchData();
  }, [isDone]);
  return (
    <>
      {!isLoading ? (
        character ? (
          <CharacterView full={character} character={JSON.parse(character.json)} setToUpdate={setChange} isEdit={true} />
        ) : (
          <Create setDone={setChange} />
        )
      ) : null}
    </>
  );
}
