import React, { Component } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import Identicon from "react-identicons";
import { Tooltip } from "@material-ui/core";
import Editor from "react-medium-editor";
import "medium-editor/dist/css/medium-editor.css";
import "medium-editor/dist/css/themes/default.css";
import "./App.css";
import NavTabs from "./NavTabs";
import LoginTab from "./LoginTab";

const client = new W3CWebSocket("ws://195.181.210.249:3000");
const contentDefaultMessage = "Start writing your document here";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUsers: [],
      userActivity: [],
      username: localStorage.username || null,
      password: localStorage.password || null,
      text: "",
    };
  }

  onLogin = user => {
    if (user.login && user.password) {
      const data = {
        username: user.login,
        password: user.password,
      };
      localStorage.username = user.login;
      localStorage.password = user.password;
      this.setState(
        {
          ...data,
        },
        () => {},
      );
    }
  };

  componentWillMount() {
    client.onopen = () => {
      console.log("WebSocket Client Connected");

      if (this.state.username) {
        const data = {
          username: this.state.username,
          password: this.state.password,
        };
        client.send(
          JSON.stringify({
            ...data,
            type: "userevent",
          }),
        );
      }
    };
  }

  render() {
    const { username } = this.state;
    return <React.Fragment>{username ? <NavTabs state={this.state} client={client} /> : <LoginTab login={this.onLogin} />}</React.Fragment>;
  }
}

export default App;
