import React, { useEffect, useState } from "react";
import { Paper, withStyles, Grid, TextField, Button, Tooltip } from "@material-ui/core";
import { Face, Fingerprint } from "@material-ui/icons";
import Identicon from "react-identicons";
import axios from "axios";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/core/styles";
import CharacterView from "./CharacterView";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function GameEditor(props) {
  const [rolls, setRolls] = useState([]);
  const [users, setUsers] = useState([]);
  const [characters, setCharacters] = useState([]);
  const [toUpdate, setToUpdate] = useState(false);
  const [session, setSession] = useState(null);
  const [image, setImage] = useState({});
  const [joined, setJoin] = useState(false);
  const classes = useStyles();

  props.client.onmessage = message => {
    const dataFromServer = JSON.parse(message.data);
    if (dataFromServer.type === "roll") {
      setRolls(dataFromServer.data.rolls);
    } else if (dataFromServer.type === "userevent") {
      console.log(dataFromServer.data.users);
      setUsers(Object.values(dataFromServer.data.users));
    } else if (dataFromServer.type === "image") {
      console.log(dataFromServer.data.users);
      setImage(dataFromServer.data.imageUrl);
    }
    // stateToChange.userActivity = dataFromServer.data.userActivity;
    // this.setState({
    //     ...stateToChange
    // });
  };

  const rollk100 = () => {
    props.client.send(
      JSON.stringify({
        type: "roll",
        username: props.username,
        content: { user: props.username, value: randomIntFromInterval(1, 100), fnc: "k100", session: session },
      }),
    );
  };

  const rollk10 = () => {
    props.client.send(
      JSON.stringify({
        type: "roll",
        username: props.username,
        content: { user: props.username, value: randomIntFromInterval(1, 10), fnc: "k10", session: session },
      }),
    );
  };

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  const joinSession = () => {
    console.log(session);
    props.client.send(
      JSON.stringify({
        username: localStorage.username,
        password: localStorage.password,
        type: "userevent",
        session: session,
      }),
    );
    setJoin(true);
  };

  const sendImage = () => {
    props.client.send(
      JSON.stringify({
        content: { session: session, url: image.url },
        username: localStorage.username,
        password: localStorage.password,
        type: "image",
        session: session,
      }),
    );
  };
  useEffect(() => {
    const fetchData = async () => {
      const res = await axios.get("http://195.181.210.249:3000/wfrp");
      setCharacters(res.data);
    };
    fetchData();
  }, [toUpdate, rolls, users]);

  return (
    <>
      <Grid container direction="row">
        <TextField input label={`Nazwa sesji`} value={session} onChange={e => setSession(e.target.value)} />
        <Button onClick={joinSession}> Dołącz </Button>
      </Grid>
      {joined && (
        <>
          <Grid container direction="row">
            {users
              .filter(user => user.session === session)
              .map(user => (
                <React.Fragment key={user.username}>
                  <Tooltip title={user.username}>
                    <span id={user.username} className="userInfo" key={user.username}>
                      <Identicon className="account__avatar" style={{ backgroundColor: user.randomcolor }} size={40} string={user.username} />
                    </span>
                  </Tooltip>
                </React.Fragment>
              ))}
          </Grid>
          <Grid container direction="row" justify="center" alignItems="center">
            <Button variant="contained" color="primary" onClick={rollk100}>
              Roll k100
            </Button>
            <Button variant="contained" color="secondary" onClick={rollk10}>
              Roll k10
            </Button>
          </Grid>

          {localStorage.isMg && (
            <Grid container direction="row">
              <TextField input label={`Fota`} value={image.url} onChange={e => setImage({ url: e.target.value })} />
              <Button onClick={sendImage}> Send </Button>
            </Grid>
          )}
          <Grid container direction="row" justify="center" alignItems="center">
            <img src={image.url} width={700} height={400} />
          </Grid>
          <Grid container direction="column">
            {rolls
              .filter(el => el.session === session)
              .slice(0, 3)
              .map((el, i) => {
                const napis = el.user + " wyrzucił " + el.value + " na kostce " + el.fnc;
                return <div key={napis + i}>{i === 0 ? <h2>{napis}</h2> : <div>{napis}</div>}</div>;
              })}
          </Grid>

          <div className={classes.root}>
            {characters.map((el, i) => {
              const character = JSON.parse(el.json);

              return (
                <ExpansionPanel key={i}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                    <Typography className={classes.heading}>{`${el.author} - ${character.name}`}</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    <CharacterView full={el} character={character} setToUpdate={setToUpdate} isEdit={false} />
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              );
            })}
          </div>
        </>
      )}
    </>
  );
}
